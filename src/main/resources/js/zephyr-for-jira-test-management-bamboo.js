AJS.$(document).ready(function() {

	attachTestConfigEvent();
	attachConfigSaveEvent();
	attachCloudConfigSaveEvent();

});

function attachTestConfigEvent() {
	AJS.$("#server-validate-button").on("click", function() {
		validateServerInfo();
	});
}


function attachDeleteConfigEvent() {
	AJS.$("a.deleteConfig").on("click", function() {
		deleteServerInfo(this);
	});
}

function attachEditConfigEvent() {
	AJS.$("a.editConfig").on("click", function() {
		editInfo(this);
	});
}

function editInfo(zephyrServerEditbutton) {
	var baseUrl = AJS.$("meta[name='application-base-url']").attr("content");
	var serverName = AJS.$('td.' + AJS.$(zephyrServerEditbutton).attr('id')).text();
	var zfjType = AJS.$(AJS.$('td.' + AJS.$(zephyrServerEditbutton).attr('id')).siblings()[1]).text();
	var button = "#server-save-button";
	AJS.$('div.cloud').hide();
	AJS.$('#act').val("edit");
	AJS.$("#zfjType").val("server");
	if (zfjType=='cloud'){
		button = "#cloud-save-button";
		AJS.$('div.cloud').show();
        AJS.$('span.sr-f').hide();
        AJS.$('span.cl-f').show();
		AJS.$("#zfjType").val(zfjType);
	}else{
        AJS.$('span.sr-f').show();
        AJS.$('span.cl-f').hide();
	}
 	AJS.$(button).off("click");
	attachSaveEditedConfigEvent(serverName);
    AJS.$.ajax({
    	cache: false,
      url: baseUrl + "/rest/zfj-admin/1.0/serverAddr?serverAddr=" + serverName,
      dataType: "json",
      success: function(config) {
    	  if (config == undefined) {
    		  return;
    	  }
			var server = config[0];
			var serverAddr = server.serverAddr
			var user = server.user
			var pass = server.pass
			var zephyrBaseUrl = server.zephyrBaseUrl
			var accessKey = server.accessKey
			var secretKey = server.secretKey

            AJS.$('form.aui').show();
            AJS.$('form.aui').find("input").clearInputs();
       		AJS.$("div.zephyr-form-error").hide();
			AJS.$("div.zephyr-form-success").hide();
			AJS.$("div.zephyr-form-validating").hide();
			AJS.$("div.zephyr-form-validating-cloud").hide();

            
            AJS.$("input#url").val(serverAddr);
            AJS.$("input#username").val(user);
            AJS.$("input#password").val(pass);
            AJS.$("input#zephyrBaseUrl").val(zephyrBaseUrl);
            AJS.$("input#accessKey").val(accessKey);
            AJS.$("input#secretKey").val(secretKey);
      }
    });
  
}

function deleteServerInfo(zephyrServerDeletebutton) {
	var baseUrl = AJS.$("meta[name='application-base-url']").attr("content");

	var serverName = AJS.$('td.' + AJS.$(zephyrServerDeletebutton).attr('id')).text();
	var serverObj = {};
	serverObj.serverAddr = serverName;
	
    AJS.$.ajax({
    	cache: false,
	      url: baseUrl + "/rest/zfj-admin/1.0/",
	      type: "DELETE",
	      contentType: "application/json",
	      data: JSON.stringify(serverObj),
	      success: function(validationMsg) {
	    	     location.reload();
	      }
	    });

}

function validateServerInfo(configSaveEdit, PreviousServerAddr) {
	
		AJS.$("div.zephyr-form-error").hide();
		AJS.$("div.zephyr-form-success").hide();


		var serverAddr = AJS.$('input#url').val();
		var user = AJS.$('#username').val();
		var pass = AJS.$('#password').val();
		var zephyrBaseUrl = AJS.$('#zephyrBaseUrl').val();
		var accessKey = AJS.$('#accessKey').val();
		var secretKey = AJS.$('#secretKey').val();
		var zfjType = AJS.$('#zfjType').val();
		var act = AJS.$('#act').val();
		var zephyrServers = [];
		var zephyrServer = {};
		if (zfjType=='cloud'){
			AJS.$("div.zephyr-form-validating-cloud").show();
		}else{
			AJS.$("div.zephyr-form-validating").show();
		}


		zephyrServer.serverAddr = serverAddr;
		zephyrServer.user = user;
		zephyrServer.pass = pass;
		zephyrServer.zephyrBaseUrl = zephyrBaseUrl;
		zephyrServer.accessKey = accessKey;
		zephyrServer.secretKey = secretKey;
		zephyrServer.zfjType = zfjType;

		zephyrServers.push(zephyrServer);

		var baseUrl = AJS.$("meta[name='application-base-url']").attr("content");

		    AJS.$.ajax({
		      cache: false,
              beforeSend: function(xhr){xhr.setRequestHeader('X-Atlassian-Token', 'no-check');},
		      url: baseUrl + "/rest/zfj-admin/1.0/CredCheck?act="+act,
		      type: "POST",
		      contentType: "application/json",
		      data: JSON.stringify(zephyrServers),
		      success: function(validationMsg) {
		    	  
		    	  AJS.$("div.zephyr-form-validating").hide();
		    	  AJS.$("div.zephyr-form-validating-cloud").hide();
		    	  if(validationMsg.statusMsg != "Connection to Zephyr has been validated") {
		    		  AJS.$("div.zephyr-form-error").text(validationMsg.statusMsg).show();
		    		  } else {
		    			  if (configSaveEdit != undefined) {
		    				  configSave(configSaveEdit, PreviousServerAddr);
		    			  } else {
		    				  AJS.$("div.zephyr-form-success").text(validationMsg.statusMsg).show();
		    			  }
		    		  } 
		      }
		    });
}
function attachConfigSaveEvent() {
	AJS.$("#server-save-button").on("click", function() {
		validateServerInfo("save");
	});

}

function attachCloudConfigSaveEvent() {
	AJS.$("#cloud-save-button").on("click", function() {
		validateServerInfo("save");
	});

}

function attachSaveEditedConfigEvent(PreviousServerAddr) {
	AJS.$("#server-save-button").on("click", function() {
		validateServerInfo("edit", PreviousServerAddr);
	});

}

function configSave(configSaveEdit, PreviousServerAddr) {
	var baseUrl = AJS.$("meta[name='application-base-url']").attr("content");
	var url = "";

	var zephyrServers = [];

	var serverAddr = AJS.$("input#url").val();
	var user = AJS.$("input#username").val();
	var pass = AJS.$("input#password").val();
	var zephyrBaseUrl = AJS.$("input#zephyrBaseUrl").val();
	var accessKey = AJS.$("input#accessKey").val();
	var secretKey = AJS.$("input#secretKey").val();
	var zfjType = AJS.$("input#zfjType").val();

	var zephyrServer = {};

	if (configSaveEdit != undefined && configSaveEdit == "edit") {
		zephyrServer.serverAddr = PreviousServerAddr + "::" + serverAddr;
		url = baseUrl + "/rest/zfj-admin/1.0/edit";
	} else {
		zephyrServer.serverAddr = serverAddr;
		url = baseUrl + "/rest/zfj-admin/1.0/"
	}
	zephyrServer.user = user;
	zephyrServer.pass = pass;
	zephyrServer.zephyrBaseUrl = zephyrBaseUrl;
	zephyrServer.accessKey = accessKey;
	zephyrServer.secretKey = secretKey;
	zephyrServer.zfjType = zfjType;
	zephyrServers.push(zephyrServer);

	AJS.$.ajax({
		url: url,
		cache: false,
		type: "PUT",
		contentType: "application/json",
		data: JSON.stringify(zephyrServers),
		success: function(validationMsg) {
			location.reload();
		},
		processData: false
	});

}