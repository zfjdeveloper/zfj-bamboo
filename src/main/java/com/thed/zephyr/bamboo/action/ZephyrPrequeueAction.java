package com.thed.zephyr.bamboo.action;

import com.atlassian.bamboo.buildqueue.manager.CustomPreBuildQueuedAction;
import com.atlassian.bamboo.v2.build.BuildContext;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;

import java.util.List;
import java.util.Map;

import static com.thed.zephyr.bamboo.utils.ApplicationConstants.PLUGIN_KEY;
import static com.thed.zephyr.bamboo.utils.ApplicationConstants.SEPERATOR;


public class ZephyrPrequeueAction implements CustomPreBuildQueuedAction {

	private PluginSettingsFactory pluginSettingsFactory;
	private BuildContext buildContext;
	
	public ZephyrPrequeueAction(PluginSettingsFactory pluginSettingsFactory) {
		this.pluginSettingsFactory = pluginSettingsFactory;
	}
	
	@Override
	public void init(BuildContext buildContext) {
		this.buildContext = buildContext;
	}

	@Override
	public BuildContext call() throws InterruptedException, Exception {
		PluginSettings settings = pluginSettingsFactory
				.createGlobalSettings();
		@SuppressWarnings("unchecked")
		List<Map<String, String>> credList=  (List<Map<String, String>>) settings.get(PLUGIN_KEY + ".zephyrcloudconfig");

		if(credList != null && credList.size()>0) {
			for (Map<String, String> map : credList) {

				String serverAddrFromStore = map.get("serverAddr");
				String userName = map.get("user");
				String password = map.get("password");
				String zephyrBaseUrl = map.get("zephyrBaseUrl");
				String accessKey = map.get("accessKey");
				String secretKey = map.get("secretKey");
				String zfjType = map.get("zfjType");

				buildContext.getBuildResult().getCustomBuildData()
						.put("password" + serverAddrFromStore,
								userName + SEPERATOR
										+ password + SEPERATOR
										+ zephyrBaseUrl + SEPERATOR
										+ accessKey + SEPERATOR
										+ secretKey + SEPERATOR
										+ zfjType
						);
			}
			buildContext.getBuildDefinition().getConfigObjects().put("zfjcred", credList);

		}
        return buildContext;	}

}
