package com.thed.zephyr.bamboo;


import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.sal.api.user.UserManager;
import com.thed.zephyr.bamboo.utils.ApplicationConstants;
import com.thed.zephyr.bamboo.utils.URLValidator;
import com.thed.zephyr.bamboo.utils.ZephyrUtil;
import com.thed.zephyr.bamboo.utils.rest.RestClient;
import com.thed.zephyr.bamboo.utils.rest.ServerInfo;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static com.thed.zephyr.bamboo.utils.ApplicationConstants.PLUGIN_KEY;

@Path("/CredCheck")
public class CredentialCheckResource {
	private final UserManager userManager;
	private final TransactionTemplate transactionTemplate;
	private PluginSettingsFactory pluginSettingsFactory;

	public CredentialCheckResource(UserManager userManager,
			PluginSettingsFactory pluginSettingsFactory,
			TransactionTemplate transactionTemplate) {
		this.userManager = userManager;
		this.transactionTemplate = transactionTemplate;
		this.pluginSettingsFactory = pluginSettingsFactory;

	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Response post(final List<Config> configs, @Context final HttpServletRequest request) {

		return Response.ok(
				transactionTemplate.execute(new TransactionCallback() {
					public Object doInTransaction() {
						Config config = configs.get(0);
						config.setStatus(false);
						config.setStatusMsg("fail");
						
						String serverAddr = config.getServerAddr();
						String user = config.getUser();
						String pass = config.getPass();
						String zfjType = config.getZfjType();
						String zephyrBaseUrl = config.getZephyrBaseUrl();
						String accessKey = config.getAccessKey();
						String secretKey = config.getSecretKey();


						if(serverAddr == null || serverAddr == ""){
							config.setStatusMsg("Please enter URL.");
							return config;
						}
						if(user == null || user == ""){
							config.setStatusMsg("Please enter Username.");
							return config;
						}
						if(pass == null || pass == ""){
							config.setStatusMsg("Please enter Password.");
							return config;
						}
						if (!(serverAddr.trim().startsWith("https://") || serverAddr
								.trim().startsWith("http://"))) {
							config.setStatusMsg("Incorrect server address format.");
							return config;
						}
						if(zfjType.equals(ApplicationConstants.CLOUD)){
							if(zephyrBaseUrl == null || zephyrBaseUrl == ""){
								config.setStatusMsg("Please enter Zephyr URL.");
								return config;
							}
							if (!(zephyrBaseUrl.trim().startsWith("https://") || zephyrBaseUrl
									.trim().startsWith("http://"))) {
								config.setStatusMsg("Incorrect cloud server address format.");
								return config;
							}
							if(accessKey == null || accessKey == ""){
								config.setStatusMsg("Please enter API AccessKey.");
								return config;
							}
							if(secretKey == null || secretKey == ""){
								config.setStatusMsg("Please enter API SecretKey.");
								return config;
							}
						}
						String zephyrURL = URLValidator.validateURL(serverAddr);
						boolean credentialValidationResultMap;
						RestClient restClient = null;
						try {
					    	restClient = getRestclient(serverAddr, user, pass, zephyrBaseUrl, zfjType, accessKey, secretKey);

							if (!zephyrURL.startsWith("http")) {
								config.setStatusMsg(zephyrURL);
								return config;
				            }

							if (!ServerInfo.findServerAddressIsValidZephyrURL(restClient)) {
								config.setStatusMsg("This is not a valid JIRA Server.");
								return config;
				            }

				            if(!request.getParameter("act").equals("edit") && checkExistingServer(restClient)){
								config.setStatusMsg("This JIRA already exist in this system.");
								return config;
							}

							credentialValidationResultMap = ServerInfo
				                    .validateCredentials(restClient);

							if(zfjType.equals(ApplicationConstants.CLOUD)) {
								Map<Boolean, String> findServerAddressIsValidZephyrCloudURL = ServerInfo.findServerAddressIsValidZephyrCloudURL(restClient);
								if (!findServerAddressIsValidZephyrCloudURL.containsKey(true)
										&& findServerAddressIsValidZephyrCloudURL.size()>0) {
									config.setStatusMsg(findServerAddressIsValidZephyrCloudURL.get(false));
									return config;
								}
							}
							if (credentialValidationResultMap && !ServerInfo.findUserHasBrowseProjectPermission(restClient)) {
									config.setStatusMsg("User has no browse project permissions");
									return config;
								}
						} finally {
							ZephyrUtil.closeHTTPClient(restClient);
						}
						if (!credentialValidationResultMap) {
							config.setStatusMsg("Validation failed");
							return config;
						}

						config.setStatus(true);
						config.setStatusMsg("Connection to Zephyr has been validated");

						return config;
					}
				})).build();
	}

	private boolean checkExistingServer(RestClient restClient) {
 		PluginSettings settings = pluginSettingsFactory
				.createGlobalSettings();

		@SuppressWarnings("unchecked")
		List<Map<String, String>> credList=  (List<Map<String, String>>) settings.get(PLUGIN_KEY + ".zephyrcloudconfig");

		if(credList != null && credList.size() > 0) {
			for (Iterator<Map<String, String>> iterator = credList.iterator(); iterator.hasNext();) {
				Map<String, String> map = iterator.next();
				String serverAddrFromStore = map.get("serverAddr");
				if (serverAddrFromStore.equals(restClient.getUrl())) {
					return true;
				}
			}
		}
		return false;
	}


	private RestClient getRestclient(String serverAddr, String user, String pass, String zephyrBaseUrl, String zfjType, String accessKey, String secretKey) {

			RestClient restClient = new RestClient(serverAddr, user, pass, zephyrBaseUrl, accessKey, secretKey);
			
			return restClient;
	}
	


	
	@XmlRootElement
	@XmlAccessorType(XmlAccessType.FIELD)
	public static final class Config {
		@XmlElement
		private String serverAddr;
		@XmlElement
		private String user;
		@XmlElement
		private String pass;
		@XmlElement
		private String zephyrBaseUrl;
		@XmlElement
		private String accessKey;
		@XmlElement
		private String secretKey;
		@XmlElement
		private String zfjType;
		@XmlElement
		private boolean status;
		@XmlElement
		private String statusMsg;

		public String getServerAddr() {
			return serverAddr;
		}

		public void setServerAddr(String serverAddr) {
			this.serverAddr = serverAddr;
		}

		public String getUser() {
			return user;
		}

		public void setUser(String user) {
			this.user = user;
		}

		public String getPass() {
			return pass;
		}

		public void setPass(String pass) {
			this.pass = pass;
		}

		public boolean isStatus() {
			return status;
		}

		public void setStatus(boolean status) {
			this.status = status;
		}

		public String getStatusMsg() {
			return statusMsg;
		}

		public void setStatusMsg(String statusMsg) {
			this.statusMsg = statusMsg;
		}

		public String getZephyrBaseUrl() {
			return zephyrBaseUrl;
		}

		public void setZephyrBaseUrl(String zephyrBaseUrl) {
			this.zephyrBaseUrl = zephyrBaseUrl;
		}

		public String getAccessKey() {
			return accessKey;
		}

		public void setAccessKey(String accessKey) {
			this.accessKey = accessKey;
		}

		public String getSecretKey() {
			return secretKey;
		}

		public void setSecretKey(String secretKey) {
			this.secretKey = secretKey;
		}

		public String getZfjType() {
			return zfjType;
		}

		public void setZfjType(String zfjType) {
			this.zfjType = zfjType;
		}
	}

}