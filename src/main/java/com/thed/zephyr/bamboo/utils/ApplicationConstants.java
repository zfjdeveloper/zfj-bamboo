package com.thed.zephyr.bamboo.utils;

public class ApplicationConstants {


    private ApplicationConstants() {
		
	}
	public static final String HEADER_USER_AGENT = "User-Agent" ;
	public static final String BAMBOO = "BAMBOO" ;
	public static final String PLUGIN_KEY = "com.thed.zephyr.zfj-bamboo";
	public static final String NAME_POST_BUILD_ACTION = "Publish test result to Zephyr Enterprise";
	public static final String ADD_ZEPHYR_GLOBAL_CONFIG = "Please Add Zephyr Server in the Global config";
	public static final String ZEPHYR_DEFAULT_MANAGER = "test.manager";
	public static final String ZEPHYR_DEFAULT_PASSWORD = "test.manager";
	public static final String NEW_CYCLE_KEY = "CreateNewCycle";
	public static final String CYCLE_PREFIX_DEFAULT = "Automation_";
	public static final String TEST_CASE_TAG = "API";
	public static final String TEST_CASE_PRIORITY = "1";
	public static final String EXTERNAL_ID = "99999";
	public static final String TEST_CASE_COMMENT = "Created via Jenkins Zephyr Plugin!";
	public static final String CYCLE_DURATION_1_DAY = "1 day";
	public static final String CYCLE_DURATION_7_DAYS = "7 days";
	public static final String CYCLE_DURATION_30_DAYS = "30 days";
	public static final String CONNECTION_ERROR = "Could not establish the connection";
	public static final String NO_SERVER_CONFIGURED = "No Zephyr server configured";
	public static final String SUCCESS = "success";
	public static final String NO_SAVE_NO_SERVER_CONFIGURED = "Cannot Save Task : No Zephyr server configured";
	public static final String NO_PROJECT = "No Project selected";
	public static final String NO_RELEASE = "No Release selected";
	public static final String NO_CYCLE = "No Cycle selected";
	
	public static final long NEW_CYCLE_KEY_IDENTIFIER = 1000000000L;
	public static final String NEW_CYCLE = "New Cycle";
	
	public static final boolean AUTOMATED = false;

	public static final String CREATE_PACKAGE = "createPackage";
	public static final String CYCLE_PREFIX = "cyclePrefix";
	public static final String CYCLE_DURATION = "cycleDuration";
	public static final String CYCLE_KEY = "cycleKey";
	public static final String RELEASE_KEY = "releaseKey";
	public static final String PROJECT_KEY = "projectKey";
	public static final String SERVER_ADDRESS = "serverAddress";
	public static final String ZEPHYR_BASEURL = "zephyrBaseUrl";
	public static final String ACCESSKEY = "accessKey";
	public static final String SECRETKEY = "secretKey";
	public static final String CLOUD = "cloud";
	public static final String URL_GET_CYCLES_CLOUD = "{SERVER}/public/rest/api/1.0/cycles/search?projectId={projectId}&versionId={versionId}";
	public static final String URL_CREATE_CYCLES_CLOUD = "{SERVER}/public/rest/api/1.0/cycle";
	public static final String URL_ZFJC_ASSIGN_TESTS = "{SERVER}/public/rest/api/1.0/executions/add/cycle/{cycleId}";
	public static final String URL_ZFJC_CREATE_EXECUTIONS_URL = "{SERVER}/public/rest/api/1.0/executions/search/cycle/{cycleId}?projectId={projectId}&versionId={versionId}&offset={offset}&action=expand";
	public static final String URL_ZFJC_EXECUTE_TEST = "{SERVER}/public/rest/api/1.0/executions";
	public static final String URL_JOB_PROGRESS_ZFJC = "{SERVER}/public/rest/api/1.0/jobprogress/{jobProgressToken}";
	public static final String HTTP_REQUEST_METHOD_GET = "GET";
	public static final String HTTP_REQUEST_METHOD_POST = "POST";
	public static final String JSON_ARRAY_START_CHAR = "[";
	public static final String JSON_ARRAY_END_CHAR = "]";
	public static final String JSON_OBJECT_START_CHAR = "{";
	public static final String JSON_OBJECT_END_CHAR = "}";
	public static final String HEADER_ZFJC_ACCESS_KEY = "zapiAccessKey";
	public static final String HEADER_AUTHORIZATION = "Authorization";
	public static final String HEADER_CONTENT_TYPE = "Content-Type";
	public static final String CONTENT_TYPE_JSON = "application/json";
	public static final String CONTENT_TYPE_TEXT = "text/plain";
	public static final String SEPERATOR = "##z##";
	public static final String ADHOC_CYCLE = "Ad hoc";
	public static final String ADHOC_CYCLE_ID = "-1";
	static final String ATLASSIAN_NET = "atlassian.net";
	static final String JIRA_COM = "jira.com";
}
