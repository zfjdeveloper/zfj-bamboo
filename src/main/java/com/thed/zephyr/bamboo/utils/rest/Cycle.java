package com.thed.zephyr.bamboo.utils.rest;

import com.thed.zephyr.bamboo.model.ZephyrConfigModel;
import com.thed.zephyr.bamboo.utils.ApplicationConstants;
import com.thed.zephyr.bamboo.utils.ZephyrUtil;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Cycle {
	private static Logger log = Logger.getLogger(Cycle.class);

	private static String URL_GET_CYCLES = "{SERVER}/rest/zapi/latest/cycle?projectId={projectId}&versionId={versionId}";
	private static String URL_CREATE_CYCLES = "{SERVER}/rest/zapi/latest/cycle";
	private static String URL_ZFJ_LICENSE = "{SERVER}/rest/zapi/latest/license";

	public static Long createCycle(ZephyrConfigModel zephyrData) {

		Long cycleId = 0L;

		HttpResponse response = null;
		String createCycleURL = null;
		try {

			createCycleURL = URL_CREATE_CYCLES.replace("{SERVER}", zephyrData.getRestClient().getUrl());

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("E dd, MMM yyyy hh:mm a");
			String dateFormatForCycleCreation = sdf.format(date);

			JSONObject jObject = new JSONObject();
			String cycleName = zephyrData.getCyclePrefix() + dateFormatForCycleCreation;

			SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MMM/yy");
			String startDate = sdf1.format(date);
			
			GregorianCalendar gCal = new GregorianCalendar();

			if (zephyrData.getCycleDuration().trim().equalsIgnoreCase("30 days")) {
				gCal.add(Calendar.DAY_OF_MONTH, +29);
			} else if (zephyrData.getCycleDuration().trim().equalsIgnoreCase("7 days")) {
				gCal.add(Calendar.DAY_OF_MONTH, +6);
			}

			String endDate = sdf1.format(gCal.getTime());
			
			jObject.put("name", cycleName);
			jObject.put("projectId", zephyrData.getZephyrProjectId());
			jObject.put("versionId", zephyrData.getVersionId());
			jObject.put("startDate", startDate);
			jObject.put("endDate", endDate);
			
			StringEntity se = new StringEntity(jObject.toString(), "utf-8");
			
			HttpPost createCycleRequest = new HttpPost(createCycleURL);
			
			createCycleRequest.setHeader("Content-Type", "application/json");
			createCycleRequest.setEntity(se);
			response = zephyrData.getRestClient().getHttpclient().execute(createCycleRequest);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		int statusCode = response.getStatusLine().getStatusCode();

		if (statusCode >= 200 && statusCode < 300) {
			HttpEntity entity = response.getEntity();
			String string = "";
			string = ZephyrUtil.printServerResponse(entity);

			try {
				JSONObject cycleObj = new JSONObject(string);
				cycleId = cycleObj.getLong("id");
				
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
			
		} else if(statusCode == 405) {

			try {
				throw new ClientProtocolException("ZAPI plugin license is invalid"
						+ statusCode);
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			}
		
		} else {
			try {
				throw new ClientProtocolException("Unexpected response status: " + statusCode);
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			}
		}
	
		return cycleId;
	}

	
	public static Map<String, String> getAllCyclesByVersionId(long versionId, RestClient restClient, String projectId) {


		Map<String, String> cycles = new TreeMap<String, String>();
		
		HttpResponse response = null;
		
		final String url = URL_GET_CYCLES.replace("{SERVER}", restClient.getUrl()).replace("{projectId}", projectId).replace("{versionId}", versionId+"");
		try {
			response = restClient.getHttpclient().execute(new HttpGet(url));
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		int statusCode = response.getStatusLine().getStatusCode();

		if (statusCode >= 200 && statusCode < 300) {
			HttpEntity entity = response.getEntity();
			String string = null;

			string = ZephyrUtil.printServerResponse(entity);

			try {
				JSONObject projObj = new JSONObject(string);
				for(int i = 0; i < projObj.length(); i++) {
					Iterator<String> keys = projObj.keys();
					
					while (keys.hasNext()) {
						String key = (String) keys.next();
						if (/*!key.trim().equals("-1") && */!key.trim().equals("recordsCount")) {
							JSONObject cycleObject = projObj.getJSONObject(key);
							String cycleName = cycleObject.getString("name");
//							long id = Long.parseLong(key);
							cycles.put(key, cycleName);
						}
					}
					
				}
				
				
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
			
		}
	
		return cycles;
	}

	public static Map<String, String> getAllCyclesByVersionIdZFJC(long versionId, RestClient restClient, String projectId) {
		Map<String, String> cycles = new TreeMap<String, String>();

		HttpResponse response = null;
		try {
			String constructedURL = ApplicationConstants.URL_GET_CYCLES_CLOUD.replace("{SERVER}", restClient.getZephyrCloudURL()).replace("{projectId}", projectId).replace("{versionId}", versionId + "");
			String jwtHeaderValue = ServerInfo.generateJWT(restClient, constructedURL, ApplicationConstants.HTTP_REQUEST_METHOD_GET);
			System.out.println(jwtHeaderValue);
			HttpGet getRequest = new HttpGet(constructedURL);

			getRequest.addHeader(ApplicationConstants.HEADER_CONTENT_TYPE, ApplicationConstants.CONTENT_TYPE_TEXT);
			getRequest.addHeader(ApplicationConstants.HEADER_AUTHORIZATION, jwtHeaderValue);
			getRequest.addHeader(ApplicationConstants.HEADER_ZFJC_ACCESS_KEY, restClient.getAccessKey());
			getRequest.addHeader(ApplicationConstants.HEADER_USER_AGENT, ApplicationConstants.BAMBOO);

			response = restClient.getHttpclient().execute(getRequest);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		int statusCode = response.getStatusLine().getStatusCode();

		if (statusCode == 400) {
			System.out.println("Bad request. Please check content type and other request parameters");
		} else if (statusCode == 401) {
			System.out.println("UnAuthorized");
		} else if (statusCode == 404) {
			System.out.println("Invalid Zephyr for JIRA Cloud URL");
		} else if (statusCode == 200) {

			HttpEntity entity = response.getEntity();
			String stringCycles = "";
			try {
				stringCycles = ZephyrUtil.printServerResponse(entity);;
				if (stringCycles != null &&
						stringCycles.startsWith(ApplicationConstants.JSON_ARRAY_START_CHAR) &&
						stringCycles.endsWith(ApplicationConstants.JSON_ARRAY_END_CHAR) && stringCycles.contains("tenantKey")) {

					try {
						JSONArray projObj = new JSONArray(stringCycles);
						for(int i = 0; i < projObj.length(); i++) {
							JSONObject cycleObj = projObj.getJSONObject(i);

							String cycleName = cycleObj.getString("name");
							String id = cycleObj.getString("id");
							cycles.put(id, cycleName);
						}


					} catch (JSONException e) {
						e.printStackTrace();
					}


				} else {
					System.out.println("Invalid cycle response : " + stringCycles);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		return cycles;
	}

    public static String createCycleZFJC(ZephyrConfigModel zephyrData) {


        String cycleId = null;

        HttpResponse response = null;
        try {

            RestClient restClient = zephyrData.getRestClient();
            String createCycleURL = ApplicationConstants.URL_CREATE_CYCLES_CLOUD.replace("{SERVER}", restClient.getZephyrCloudURL());
            String jwtHeaderValue = ServerInfo.generateJWT(restClient, createCycleURL, ApplicationConstants.HTTP_REQUEST_METHOD_POST);
			System.out.println(jwtHeaderValue);
            Date date = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("E dd, yyyy hh:mm a");
            String dateFormatForCycleCreation = sdf.format(date);

            JSONObject jObject = new JSONObject();
            String cycleName = zephyrData.getCyclePrefix() + dateFormatForCycleCreation;

            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
            String startDate = sdf1.format(date);

            GregorianCalendar gCal = new GregorianCalendar();

            if (zephyrData.getCycleDuration().trim().equalsIgnoreCase("30 days")) {
                gCal.add(Calendar.DAY_OF_MONTH, +29);
            } else if (zephyrData.getCycleDuration().trim().equalsIgnoreCase("7 days")) {
                gCal.add(Calendar.DAY_OF_MONTH, +6);
            }

            String endDate = sdf1.format(gCal.getTime());

            jObject.put("name", cycleName);
            jObject.put("projectId", zephyrData.getZephyrProjectId());
            jObject.put("versionId", zephyrData.getVersionId());
            jObject.put("startDate", date.getTime());
            jObject.put("endDate", gCal.getTimeInMillis());

            StringEntity se = new StringEntity(jObject.toString(), "utf-8");

            HttpPost createCycleRequest = new HttpPost(createCycleURL);

            createCycleRequest.addHeader(ApplicationConstants.HEADER_CONTENT_TYPE, ApplicationConstants.CONTENT_TYPE_JSON);
            createCycleRequest.addHeader(ApplicationConstants.HEADER_AUTHORIZATION, jwtHeaderValue);
            createCycleRequest.addHeader(ApplicationConstants.HEADER_ZFJC_ACCESS_KEY, restClient.getAccessKey());
            createCycleRequest.addHeader(ApplicationConstants.HEADER_USER_AGENT, ApplicationConstants.BAMBOO);

            createCycleRequest.setEntity(se);
            response = restClient.getHttpclient().execute(createCycleRequest);
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        int statusCode = response.getStatusLine().getStatusCode();
		String string = null;
		string = ZephyrUtil.printServerResponse(response.getEntity());
        if (statusCode >= 200 && statusCode < 300) {

            try {
                JSONObject cycleObj = new JSONObject(string);
                cycleId = cycleObj.getString("id");

            } catch (JSONException e) {
                e.printStackTrace();
            }


        } else {
            try {
                throw new ClientProtocolException(string + statusCode);
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            }
        }

        return cycleId;

    }

}
