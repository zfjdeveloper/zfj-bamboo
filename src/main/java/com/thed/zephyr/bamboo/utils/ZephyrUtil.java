package com.thed.zephyr.bamboo.utils;

import com.thed.zephyr.bamboo.utils.rest.RestClient;
import org.apache.http.HttpEntity;
import org.apache.http.ParseException;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import java.io.IOException;

public class ZephyrUtil {
	private static Logger log = Logger.getLogger(ZephyrUtil.class);
	public static String findIpAddress(String URL) {

		String ipAddressPort = IPAddressPortMatcher.getIpAddressPort(URL);
		
		String[] split = ipAddressPort.split(":");
		
		return split[0];
		
		}
	
	public static int findPort(String URL) {

		int portNumber;
		String ipAddressPort = IPAddressPortMatcher.getIpAddressPort(URL);
		
		String[] split = ipAddressPort.split(":");
		
		if (split.length > 1) {
			portNumber = Integer.parseInt(split[1]);
		} else {
			portNumber = 80;
		}

		return portNumber;

	}

	/**
	 * Close HTTP Client
	 * @param restClient
	 */
	public static void closeHTTPClient(RestClient restClient) {
		if (restClient != null) {
			restClient.destroy();
		}
	}

	/**
	 * Print ZFJ Server/Cloud Response
	 * @param entity
	 * @return
	 */
	public static String printServerResponse(HttpEntity entity) {
		String string = "";
		try {
			string = EntityUtils.toString(entity);
			log.debug("Response: "+string);
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return string;
	}
}
