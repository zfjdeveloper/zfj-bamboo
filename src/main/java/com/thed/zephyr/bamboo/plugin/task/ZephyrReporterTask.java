package com.thed.zephyr.bamboo.plugin.task;

/**
 * @author mohan.kumar
 */

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.results.tests.TestResults;
import com.atlassian.bamboo.task.*;
import com.atlassian.bamboo.v2.build.CurrentBuildResult;
import com.thed.zephyr.bamboo.model.TestCaseResultModel;
import com.thed.zephyr.bamboo.model.ZephyrConfigModel;
import com.thed.zephyr.bamboo.model.ZephyrInstance;
import com.thed.zephyr.bamboo.utils.ApplicationConstants;
import com.thed.zephyr.bamboo.utils.rest.RestClient;
import com.thed.zephyr.bamboo.utils.rest.ServerInfo;
import com.thed.zephyr.bamboo.utils.rest.TestCaseUtil;
import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;

import java.util.*;

import static com.thed.zephyr.bamboo.utils.ApplicationConstants.*;

public class ZephyrReporterTask implements TaskType {


	public ZephyrReporterTask() { }

	@Override
	public TaskResult execute(TaskContext taskContext) throws TaskException {
		BuildLogger buildLogger = taskContext.getBuildLogger();
		TaskResultBuilder taskResultBuilder = TaskResultBuilder.newBuilder(taskContext);
		
		 CurrentBuildResult buildResult = ((TaskContext) taskContext).getBuildContext().getBuildResult();

		if (buildResult == null
				|| ((buildResult.getSuccessfulTestResults() == null || buildResult.getSuccessfulTestResults().isEmpty()) && buildResult
						.getFailedTestResults() == null)
				|| ((buildResult.getFailedTestResults() == null || buildResult.getFailedTestResults().isEmpty()) && buildResult
						.getFailedTestResults() == null)){
//				|| (buildResult.getBuildErrors() != null && buildResult.getBuildErrors().size()>0)) {
			buildLogger.addBuildLogEntry("Error parsing surefire reports.");
			buildLogger.addBuildLogEntry("Please ensure \"Publish JUnit test result report is added\" as a post build action");
			return taskResultBuilder.failed().build();
    	}

		
		buildLogger.addBuildLogEntry("##########################################################");
		buildLogger.addBuildLogEntry("Zephyr for JIRA Test Result Reporter is processing the results");
		buildLogger.addBuildLogEntry("##########################################################");
		
		ConfigurationMap config = taskContext.getConfigurationMap();
		
		 String serverAddress = config.get(SERVER_ADDRESS).trim();
		 String projectKey = config.get(PROJECT_KEY).trim();
		 String releaseKey = config.get(RELEASE_KEY).trim();
		 String cycleKey = config.get(CYCLE_KEY).trim();
		 String cycleDuration = config.get(CYCLE_DURATION).trim();
		 String cyclePrefix = config.get(CYCLE_PREFIX).trim();
		 
		 buildLogger.addBuildLogEntry("Server Address :" + serverAddress);
		 buildLogger.addBuildLogEntry("Project id : " + projectKey);
		 buildLogger.addBuildLogEntry("Version id : " + releaseKey);
		 if (cycleKey.equals(NEW_CYCLE_KEY_IDENTIFIER+"")) {
			 buildLogger.addBuildLogEntry("New cycle is selected");
		 } else {
			 buildLogger.addBuildLogEntry("Cycle id : " + cycleKey);
		 }
		 buildLogger.addBuildLogEntry("Cycle duration : " + cycleDuration);
		 buildLogger.addBuildLogEntry("Cycle prefix : " + cyclePrefix);
		 
			if (!validateBuildConfig(serverAddress, projectKey, releaseKey, cycleKey)) {
				buildLogger.addBuildLogEntry("Cannot Proceed. Please verify the job configuration");
				return taskResultBuilder.failed().build();
			}

			ZephyrConfigModel zephyrConfig = initializeZephyrData(taskContext, serverAddress, projectKey, releaseKey, cycleKey, cycleDuration, cyclePrefix);
		

			if(!ServerInfo.findUserHasBrowseProjectPermission(zephyrConfig.getRestClient())) {
				buildLogger.addBuildLogEntry("User has no browse projects permission, hence cannot upload results");
				zephyrConfig.getRestClient().destroy();
				return taskResultBuilder.failed().build();
				
			}
			prepareZephyrTests(taskContext, zephyrConfig);
			
			try {
				TestCaseUtil.processTestCaseDetails(zephyrConfig);

	            zephyrConfig.getRestClient().destroy();
	            buildLogger.addBuildLogEntry("Done");

			} catch (Exception e) {
				e.printStackTrace();
				buildLogger.addBuildLogEntry("Done processing");
				zephyrConfig.getRestClient().destroy();
				return taskResultBuilder.failed().build();

			}
			
			
		 buildLogger.addBuildLogEntry("Done processing");
		return taskResultBuilder.success().build();
	}

	/**
	 * @param serverAddress
	 * @param projectKey
	 * @param releaseKey
	 * @param cycleKey
	 */
	private boolean validateBuildConfig(String serverAddress, String projectKey,
			String releaseKey, String cycleKey) {
		boolean valid = true;
		if (StringUtils.isBlank(serverAddress)
				|| StringUtils.isBlank(projectKey)
				|| StringUtils.isBlank(releaseKey)
				|| StringUtils.isBlank(cycleKey)
				|| ADD_ZEPHYR_GLOBAL_CONFIG.equals(serverAddress.trim())
				|| ADD_ZEPHYR_GLOBAL_CONFIG.equals(projectKey.trim())
				|| ADD_ZEPHYR_GLOBAL_CONFIG.equals(releaseKey.trim())
				|| ADD_ZEPHYR_GLOBAL_CONFIG.equals(cycleKey.trim())) {
			valid = false;
		}
		
		return valid;
	}


	private void prepareZephyrTests(CommonTaskContext taskContext,
			ZephyrConfigModel zephyrConfig) {
		Map<String, Boolean> zephyrTestCaseMap = prepareTestResults(taskContext);
 

		taskContext.getBuildLogger().addBuildLogEntry("Total Test Cases : " + zephyrTestCaseMap.size());
		List<TestCaseResultModel> testcases = new ArrayList<TestCaseResultModel>();

		
		Set<String> keySet = zephyrTestCaseMap.keySet();
		
		for (Iterator<String> iterator = keySet.iterator(); iterator.hasNext();) {
			String testCaseName = iterator.next();
			Boolean isPassed = zephyrTestCaseMap.get(testCaseName);
			
			
			JSONObject isssueType = new JSONObject();
			isssueType.put("id", zephyrConfig.getTestIssueTypeId()+"");
			
			JSONObject project = new JSONObject();
			project.put("id", zephyrConfig.getZephyrProjectId());

			JSONObject fields = new JSONObject();
			fields.put("project", project);
			fields.put("summary", testCaseName);
			fields.put("description", "Creating the Test via Bamboo");
			fields.put("issuetype", isssueType);
			
			JSONObject issue = new JSONObject();
			issue.put("fields", fields);
			
			TestCaseResultModel caseWithStatus = new TestCaseResultModel();
			caseWithStatus.setPassed(isPassed);
			caseWithStatus.setTestCase(issue.toString());
			caseWithStatus.setTestCaseName(testCaseName);
			testcases.add(caseWithStatus);
		}
		
		zephyrConfig.setTestcases(testcases);
	}

	/**
	 * @param taskContext
	 * @return
	 */
	private Map<String, Boolean> prepareTestResults(
			CommonTaskContext taskContext) {
		Map<String, Boolean> zephyrTestCaseMap = new HashMap<String, Boolean>();

		
 CurrentBuildResult buildResult = ((TaskContext) taskContext).getBuildContext().getBuildResult();
 Collection<TestResults> failedTestResults = buildResult.getFailedTestResults();
 Collection<TestResults> passedTestResults = buildResult.getSuccessfulTestResults();

 for (Iterator<TestResults> iterator = failedTestResults.iterator(); iterator
			.hasNext();) {
		TestResults testResults = iterator.next();
		zephyrTestCaseMap.put(testResults.getClassName()+"."+testResults.getActualMethodName(), false);
		
}
 
 for (Iterator<TestResults> iterator = passedTestResults.iterator(); iterator
				.hasNext();) {
			TestResults testResults = iterator.next();
			zephyrTestCaseMap.put(testResults.getClassName()+"."+testResults.getActualMethodName(), true);
			
		}
		return zephyrTestCaseMap;
	}

	/**
	 * @param serverAddress
	 * @param projectKey
	 * @param releaseKey
	 * @param cycleKey
	 * @param cycleDuration
	 * @param cyclePrefix
	 * @return
	 */
	private ZephyrConfigModel initializeZephyrData(TaskContext taskContext, String serverAddress, String projectKey, String releaseKey, String cycleKey, String cycleDuration,
			 String cyclePrefix) {
		
		 ZephyrInstance instance = prepareUserCredentials(taskContext, serverAddress);


		ZephyrConfigModel zephyrConfig = new ZephyrConfigModel();
		zephyrConfig.setRestClient(new RestClient(instance));

		prepareRestClient(taskContext, zephyrConfig, serverAddress);


		zephyrConfig.setZephyrProjectId(Long.parseLong(projectKey));
		zephyrConfig.setVersionId(Long.parseLong(releaseKey));
		determineCycleID(zephyrConfig, cycleKey);
		zephyrConfig.setCycleDuration(cycleDuration);
		zephyrConfig.setCyclePrefix(determineCyclePrefix(cyclePrefix));
		
        determineTestIssueTypeId(taskContext, zephyrConfig);

		return zephyrConfig;
	}

	private void prepareRestClient(TaskContext taskContext, ZephyrConfigModel zephyrConfig, String serverAddr) {
		ZephyrInstance instance = new ZephyrInstance();
		RestClient restClient = null;
		Map<String, String> customBuildData = taskContext.getBuildContext().getBuildResult().getCustomBuildData();

		if(customBuildData.containsKey("password" + serverAddr)) {
			String[] split = customBuildData.get("password" + serverAddr).split(SEPERATOR);
			String userName = split[0];
			String password = split[1];
			String zephyrBaseUrl = split[2];
			String accessKey = split[3];;
			String secretKey = split[4];;
			String zfjType = split[5];;

			instance.setServerAddress(serverAddr);
			instance.setUsername(userName);
			instance.setPassword(password);
			instance.setZephyrBaseUrl(zephyrBaseUrl);
			instance.setAccessKey(accessKey);
			instance.setSecretKey(secretKey);
			instance.setZfjType(zfjType);
			
			if(zfjType.equals(ApplicationConstants.CLOUD)) {
				zephyrConfig.setZfjClud(true);
				restClient = new RestClient(serverAddr,userName, password, zephyrBaseUrl, accessKey, secretKey, zfjType);
			} else {
				zephyrConfig.setZfjClud(false);
				restClient = new RestClient(serverAddr,userName, password);
			}
		}


		zephyrConfig.setRestClient(restClient);
	}

	private void determineTestIssueTypeId(TaskContext taskContext, final ZephyrConfigModel zephyrConfig) {
		long testIssueTypeId = ServerInfo.findTestIssueTypeId(zephyrConfig.getRestClient());
		zephyrConfig.setTestIssueTypeId(testIssueTypeId);
			
	}

	private String determineCyclePrefix(String cyclePrefix) {
		if (StringUtils.isNotBlank(cyclePrefix)) {
			return cyclePrefix + "_";
		} else {
			return CYCLE_PREFIX_DEFAULT;
		}
	}

	private ZephyrInstance prepareUserCredentials(TaskContext taskContext, String serverAddr) {
		ZephyrInstance instance = new ZephyrInstance();

		Map<String, String> customBuildData = taskContext.getBuildContext().getBuildResult().getCustomBuildData();

		if(customBuildData.containsKey("password" + serverAddr)) {
			String[] split = customBuildData.get("password" + serverAddr).split(SEPERATOR);
			instance = new ZephyrInstance();
			String userName = split[0];
			String password = split[1];
			String zephyrBaseUrl = split[2];
			String accessKey = split[3];
			String secretKey = split[4];
			String zfjType = split[5];

			instance.setServerAddress(serverAddr);
			instance.setUsername(userName);
			instance.setPassword(password);
			instance.setZephyrBaseUrl(zephyrBaseUrl);
			instance.setAccessKey(accessKey);
			instance.setSecretKey(secretKey);
			instance.setZfjType(zfjType);

		}

		
		return instance;
	}
	
	public static String findPackageName(String testCaseNameWithPackage) {
		String packageName = "";

		if (StringUtils.isBlank(testCaseNameWithPackage)) {
			return packageName;
		}

		String[] split = testCaseNameWithPackage.split("\\.");

		int splitCount = split.length;

		for (int i = 0; i < split.length; i++) {
			if (splitCount > 2) {

				if (i == splitCount - 2)
					break;

				packageName += split[i];
				packageName += ".";
			}
		}

		String removeEnd = StringUtils.removeEnd(packageName, ".");
		return removeEnd;
	}
	
private void determineCycleID(ZephyrConfigModel zephyrConfig, String cycleKey) {
		
		if(zephyrConfig.isZfjClud()) {
			
			if (cycleKey.equalsIgnoreCase(NEW_CYCLE_KEY)) {
				zephyrConfig.setCycleId(NEW_CYCLE_KEY_IDENTIFIER);
				zephyrConfig.setCycleIdZfjCloud(NEW_CYCLE_KEY_IDENTIFIER+"");
				return;
			}

			zephyrConfig.setCycleName(cycleKey);	
			zephyrConfig.setCycleIdZfjCloud(cycleKey);
			return;
		}
		if (cycleKey.equalsIgnoreCase(NEW_CYCLE_KEY)) {
			zephyrConfig.setCycleId(NEW_CYCLE_KEY_IDENTIFIER);
			return;
		}
		long cycleId = 0;
		try {
			cycleId = Long.parseLong(cycleKey);
		} catch (NumberFormatException e1) {
			System.out.println("Cycle Key appears to be the name of the cycle");
			e1.printStackTrace();
		}
		zephyrConfig.setCycleName(cycleKey);	
		zephyrConfig.setCycleId(cycleId);
	}
}
