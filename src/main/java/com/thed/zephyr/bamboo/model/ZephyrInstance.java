package com.thed.zephyr.bamboo.model;

public class ZephyrInstance {

	private String serverAddress;
	private String zephyrBaseUrl;
	private String accessKey;
	private String secretKey;
	private String zfjType;
	private String username;
	private String password;
	
	public String getServerAddress() {
		return serverAddress;
	}

	public void setServerAddress(String serverAddress) {
		this.serverAddress = serverAddress;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getZephyrBaseUrl() {
		return zephyrBaseUrl;
	}

	public void setZephyrBaseUrl(String zephyrBaseUrl) {
		this.zephyrBaseUrl = zephyrBaseUrl;
	}

	public String getAccessKey() {
		return accessKey;
	}

	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}

	public String getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

	public String getZfjType() {
		return zfjType;
	}

	public void setZfjType(String zfjType) {
		this.zfjType = zfjType;
	}
}
