package com.thed.zephyr.bamboo;

import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.sal.api.user.UserManager;
import com.thed.zephyr.bamboo.utils.rest.RestClient;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.*;

import static com.thed.zephyr.bamboo.utils.ApplicationConstants.PLUGIN_KEY;

@Path("/")
public class ConfigResource {
	private final UserManager userManager;
	private static PluginSettingsFactory pluginSettingsFactory;
	private final TransactionTemplate transactionTemplate;

	public ConfigResource(UserManager userManager,
			PluginSettingsFactory pluginSettingsFactory,
			TransactionTemplate transactionTemplate) {
		this.userManager = userManager;
		ConfigResource.pluginSettingsFactory = pluginSettingsFactory;
		this.transactionTemplate = transactionTemplate;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@Context HttpServletRequest request) {

		return Response.ok(
				transactionTemplate.execute(new TransactionCallback() {
					public Object doInTransaction() {
						PluginSettings settings = pluginSettingsFactory
								.createGlobalSettings();
						
						List<CredentialData> configs = new ArrayList<CredentialData>();
						@SuppressWarnings("unchecked")
						List<Map<String, String>> credList=  (List<Map<String, String>>) settings.get(PLUGIN_KEY + ".zephyrcloudconfig");
						if (credList == null || credList.size() == 0) {
							return null;
						}
						for (Iterator<Map<String, String>> iterator = credList.iterator(); iterator.hasNext();) {
							Map<String, String> map = iterator.next();
							
							CredentialData cData = new CredentialData();

							cData.setServerAddr(map.get("serverAddr"));
							cData.setUser(map.get("user"));
							cData.setPass(map.get("password"));
							cData.setZephyrBaseUrl(map.get("zephyrBaseUrl"));
							cData.setAccessKey(map.get("accessKey"));
							cData.setSecretKey(map.get("secretKey"));
							cData.setZfjType(map.get("zfjType"));
							cData.setStatusMsg(map.get("statusMsg"));
							cData.setStatus(true);
							configs.add(cData);
						}

						return configs;
					}
				})).build();
	}

	
	
	
	@GET
	@Path(value = "/serverAddr")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getServerConfigByServer(@Context HttpServletRequest request, @QueryParam("serverAddr") final String serverAddr) {

		return Response.ok(
				transactionTemplate.execute(new TransactionCallback() {
					public Object doInTransaction() {
						PluginSettings settings = pluginSettingsFactory
								.createGlobalSettings();
						
						List<CredentialData> configs = new ArrayList<CredentialData>();
						@SuppressWarnings("unchecked")
						List<Map<String, String>> credList=  (List<Map<String, String>>) settings.get(PLUGIN_KEY + ".zephyrcloudconfig");
						if (credList == null || credList.size() == 0) {
							return null;
						}
						for (Iterator<Map<String, String>> iterator = credList.iterator(); iterator.hasNext();) {
							Map<String, String> map = iterator.next();
							
							
							if(map.get("serverAddr").equals(serverAddr.trim())) {
								CredentialData cData = new CredentialData();
								
								cData.setServerAddr(map.get("serverAddr"));
								cData.setUser(map.get("user"));
								cData.setPass(map.get("password"));
								cData.setZephyrBaseUrl(map.get("zephyrBaseUrl"));
								cData.setAccessKey(map.get("accessKey"));
								cData.setSecretKey(map.get("secretKey"));
								cData.setZfjType(map.get("zfjType"));
								cData.setStatusMsg(map.get("statusMsg"));
								cData.setStatus(true);
								configs.add(cData);
								
								break;
							}
						}

						return configs;
					}
				})).build();
	}
	
	
	
	
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response put(final List<CredentialData> configs, @Context HttpServletRequest request)
 {

		return Response.ok(
				transactionTemplate.execute(new TransactionCallback() {
					@SuppressWarnings("unchecked")
					public Object doInTransaction() {
						PluginSettings pluginSettings = pluginSettingsFactory
								.createGlobalSettings();
						List<CredentialData> configsToBePersisted = new ArrayList<CredentialData>();
						List<CredentialData> configsMsg = new ArrayList<CredentialData>();

						for (Iterator<CredentialData> iterator = configs
								.iterator(); iterator.hasNext();) {
							CredentialData credentialData = iterator.next();
							configsMsg.add(credentialData);
							String serverAddr = credentialData.getServerAddr();
							String user = credentialData.getUser();
							String pass = credentialData.getPass();
							String zephyrBaseUrl = credentialData.getZephyrBaseUrl();
							String accessKey = credentialData.getAccessKey();
							String secretKey = credentialData.getSecretKey();
							String zfjType = credentialData.getZfjType();

							credentialData.setServerAddr(serverAddr);
							credentialData.setStatus(true);
							credentialData
									.setStatusMsg("Connection to Zephyr has been validated");

							configsToBePersisted.add(credentialData);

						}
						List<Map<String, String>> credList = null;

						credList = (List<Map<String, String>>) pluginSettings
								.get(PLUGIN_KEY + ".zephyrcloudconfig");

						if (credList == null || credList.size() == 0) {
							credList = new ArrayList<Map<String, String>>();
						}

						for (CredentialData credData : configsToBePersisted) {
							String serverAddr = credData.getServerAddr();
							String user = credData.getUser();
							String password = credData.getPass();
							String statusMsg = credData.getStatusMsg();
							Map<String, String> credMap = new HashMap<String, String>();
							credMap.put("serverAddr", serverAddr);
							credMap.put("user", user);
							credMap.put("password", password);
							credMap.put("zephyrBaseUrl", credData.getZephyrBaseUrl());
							credMap.put("accessKey", credData.getAccessKey());
							credMap.put("secretKey", credData.getSecretKey());
							credMap.put("zfjType", credData.getZfjType());
							credMap.put("statusMsg", statusMsg);

							boolean exists = false;
							for (Map<String, String> cl : credList) {
								if (cl.get("serverAddr").trim()
										.equalsIgnoreCase(serverAddr.trim())) {
									exists = true;
									break;
								}
							}

							if (!exists) {
								credList.add(credMap);
							}
						}

						pluginSettings.put(PLUGIN_KEY + ".zephyrcloudconfig",
								credList);
						return configsMsg;
					}
				})).build();
	}
	
	
	@PUT
	@Path(value = "/edit")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response saveEditedConfig(final List<CredentialData> configs, @Context HttpServletRequest request)
	{

	  return Response.ok(transactionTemplate.execute(new TransactionCallback()
	  {
	    @SuppressWarnings("unchecked")
		public Object doInTransaction()
	    {
	      PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();
	      List<CredentialData> configsMsg = new ArrayList<CredentialData>();
	      
			CredentialData credentialData = configs.get(0);
			String oldServerAddr = credentialData.getServerAddr().split("::")[0];

			String serverAddr = credentialData.getServerAddr().split("::")[1];
			String user = credentialData.getUser();
			String pass = credentialData.getPass();
			String zephyrBaseUrl = credentialData.getZephyrBaseUrl();
			String accessKey = credentialData.getAccessKey();
			String secretKey = credentialData.getSecretKey();
			String zfjType = credentialData.getZfjType();

	      List<Map<String, String>> credList = null;
	      
			credList=  (List<Map<String, String>>) pluginSettings.get(PLUGIN_KEY + ".zephyrcloudconfig");
			
			if(credList == null || credList.size() == 0) {
				credList = new ArrayList<Map<String,String>>();
			}

	    	  Map<String, String> credMap = new HashMap<String, String>();
	    	  credMap.put("serverAddr", serverAddr);
	    	  credMap.put("user", user);
	    	  credMap.put("password", pass);
	    	  credMap.put("zephyrBaseUrl", zephyrBaseUrl);
	    	  credMap.put("accessKey", accessKey);
	    	  credMap.put("secretKey", secretKey);
	    	  credMap.put("zfjType", zfjType);
	    	  credMap.put("statusMsg", "Connection to Zephyr has been validated");
	    	  
	    	  for(Map<String, String> cl: credList) {
	    		  if(cl.get("serverAddr").trim().equalsIgnoreCase(oldServerAddr.trim())) {
	    			  credList.remove(cl);
	    			  break;
	    		  }
	    	  }
	    		  credList.add(credMap);
	      
	      pluginSettings.put(PLUGIN_KEY + ".zephyrcloudconfig", credList);
	      return configsMsg;
	    }
	  })
	  ).build();
	}	

	public static List<CredentialData> getServerConfig() {
		PluginSettings settings = pluginSettingsFactory
				.createGlobalSettings();
		
		List<CredentialData> configs = new ArrayList<CredentialData>();
		@SuppressWarnings("unchecked")
		List<Map<String, String>> credList=  (List<Map<String, String>>) settings.get(PLUGIN_KEY + ".zephyrcloudconfig");
		
		for (Iterator<Map<String, String>> iterator = credList.iterator(); iterator.hasNext();) {
			Map<String, String> map = iterator.next();
			
			CredentialData cData = new CredentialData();

			cData.setServerAddr(map.get("serverAddr"));
			cData.setUser(map.get("user"));
			cData.setPass(map.get("password"));
			cData.setZephyrBaseUrl(map.get("zephyrBaseUrl"));
			cData.setAccessKey(map.get("accessKey"));
			cData.setSecretKey(map.get("secretKey"));
			cData.setZfjType(map.get("zfjType"));
			cData.setStatusMsg(map.get("statusMsg"));
			cData.setStatus(true);
			configs.add(cData);
		}

		return configs;

	}

	private static RestClient getRestclient(String serverAddr) {

		RestClient restClient = null;
		
		PluginSettings settings = pluginSettingsFactory
				.createGlobalSettings();
		
		@SuppressWarnings("unchecked")
		List<Map<String, String>> credList=  (List<Map<String, String>>) settings.get(PLUGIN_KEY + ".zephyrcloudconfig");
		
		if(credList != null && credList.size() > 0) {
			for (Iterator<Map<String, String>> iterator = credList.iterator(); iterator.hasNext();) {
				Map<String, String> map = iterator.next();
				String serverAddrFromStore = map.get("serverAddr");
				
				if (serverAddrFromStore.equals(serverAddr)) {
					String userName = map.get("user");
					String password = map.get("password");
					String zephyrBaseUrl = map.get("zephyrBaseUrl");
					String zfjType = map.get("zfjType");
					String accessKey = map.get("accessKey");
					String secretKey = map.get("secretKey");
					//TODO
					restClient = new RestClient(serverAddr, userName, password, zephyrBaseUrl, accessKey, secretKey);
					//TODO : create zephyr client
				}
			}
		}


		return restClient;
	}



	public static CredentialData getCredentialData(String serverAddr) {

		CredentialData credData = null;
		List<CredentialData> serverConfig = getServerConfig();
		for (CredentialData config: serverConfig) {
			
			if(config.getServerAddr().equals(serverAddr)) {
				credData = config;
				break;
			}
			
		}

		return credData;
	}

	
	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response delete(final CredentialData configs, @Context HttpServletRequest request)
	{

	  return Response.ok(transactionTemplate.execute(new TransactionCallback()
	  {
	    @SuppressWarnings("unchecked")
		public Object doInTransaction()
	    {
	      PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();

	      List<Map<String, String>> credList = null;
	      
			credList=  (List<Map<String, String>>) pluginSettings.get(PLUGIN_KEY + ".zephyrcloudconfig");
			
			if(credList == null || credList.size() == 0) {
				credList = new ArrayList<Map<String,String>>();
			}

	    	  for(Map<String, String> cl: credList) {
	    		  if(cl.get("serverAddr").trim().equalsIgnoreCase(configs.getServerAddr().trim())) {
	    			  credList.remove(cl);
	    			  break;
	    		  }
	    	  }
	    	  
	      pluginSettings.put(PLUGIN_KEY + ".zephyrcloudconfig", credList);
	      return true;
	    }
	  })
	  ).build();
	}
}